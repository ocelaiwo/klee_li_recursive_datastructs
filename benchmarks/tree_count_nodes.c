#include <klee/klee.h>

#include <stdio.h>
#include <stdlib.h>

typedef struct _node {
  int data;
  struct _node *left;
  struct _node *right;
} node;

int count_nodes(node *n) {
  if (n == NULL)
    return 0;
  return 1 + count_nodes(n->left) + count_nodes(n->right);
}

int main(int argc, char **argv) {
  node *root;
  klee_make_symbolic(&root, sizeof(root), "root");
  int a = count_nodes(root);
  if (a == 4) {
    return 1;
  } else {
    return 0;
  }
}
