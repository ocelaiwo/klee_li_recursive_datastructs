#include "klee/klee.h"

int get_sign(int a) {
  if (a > 0) {
    return 1;
  } else if (a == 0) {
    return 2;
  } else {
    return 3;
  }
}

int main() {
  int x;
  klee_make_symbolic(&x, sizeof(x), "x");
  return get_sign(x);
}
