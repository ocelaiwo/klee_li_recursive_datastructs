#include "klee/klee.h"

#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct avl_node_s {
  struct avl_node_s *left;
  struct avl_node_s *right;
  int value;
};

typedef struct avl_node_s avl_node_t;

struct avl_tree_s {
  struct avl_node_s *root;
};

typedef struct avl_tree_s avl_tree_t;

avl_node_t *avl_create_node() {
  avl_node_t *node = NULL;

  if ((node = malloc(sizeof(avl_node_t))) == NULL) {
    return NULL;
  }

  node->left = NULL;
  node->right = NULL;
  node->value = 0;

  return node;
}

int avl_node_height(avl_node_t *node) {
  int height_left = 0;
  int height_right = 0;

  if (node->left)
    height_left = avl_node_height(node->left);
  if (node->right)
    height_right = avl_node_height(node->right);

  return height_right > height_left ? ++height_right : ++height_left;
}

avl_node_t *avl_rotate_leftleft(avl_node_t *node) {
  avl_node_t *a = node;
  avl_node_t *b = a->left;

  a->left = b->right;
  b->right = a;

  return (b);
}

avl_node_t *avl_rotate_leftright(avl_node_t *node) {
  avl_node_t *a = node;
  avl_node_t *b = a->left;
  avl_node_t *c = b->right;

  a->left = c->right;
  b->right = c->left;
  c->left = b;
  c->right = a;

  return (c);
}

avl_node_t *avl_rotate_rightleft(avl_node_t *node) {
  avl_node_t *a = node;
  avl_node_t *b = a->right;
  avl_node_t *c = b->left;

  a->right = c->left;
  b->left = c->right;
  c->right = b;
  c->left = a;

  return (c);
}

avl_node_t *avl_rotate_rightright(avl_node_t *node) {
  avl_node_t *a = node;
  avl_node_t *b = a->right;

  a->right = b->left;
  b->left = a;

  return (b);
}

int avl_balance_factor(avl_node_t *node) {
  int bf = 0;

  if (node->left)
    bf += avl_node_height(node->left);
  if (node->right)
    bf -= avl_node_height(node->right);

  return bf;
}

avl_node_t *avl_balance_node(avl_node_t *node) {
  avl_node_t *newroot = NULL;

  /* Balance our children, if they exist. */
  if (node->left)
    node->left = avl_balance_node(node->left);
  if (node->right)
    node->right = avl_balance_node(node->right);

  int bf = avl_balance_factor(node);

  if (bf >= 2) {
    /* Left Heavy */

    if (avl_balance_factor(node->left) <= -1)
      newroot = avl_rotate_leftright(node);
    else
      newroot = avl_rotate_leftleft(node);

  } else if (bf <= -2) {
    /* Right Heavy */

    if (avl_balance_factor(node->right) >= 1)
      newroot = avl_rotate_rightleft(node);
    else
      newroot = avl_rotate_rightright(node);

  } else {
    /* This node is balanced -- no change. */

    newroot = node;
  }

  return (newroot);
}

void avl_balance(avl_tree_t *tree) {

  avl_node_t *newroot = NULL;

  newroot = avl_balance_node(tree->root);

  if (newroot != tree->root) {
    tree->root = newroot;
  }
}

void avl_insert(avl_tree_t *tree, int value) {
  avl_node_t *node = NULL;
  avl_node_t *next = NULL;
  avl_node_t *last = NULL;

  /* Well, there must be a first case */
  if (tree->root == NULL) {
    node = avl_create_node();
    node->value = value;

    tree->root = node;

    /* Okay.  We have a root already.  Where do we put this? */
  } else {
    next = tree->root;

    while (next != NULL) {
      last = next;

      if (value < next->value) {
        next = next->left;

      } else if (value > next->value) {
        next = next->right;

        /* Have we already inserted this node? */
      } else if (value == next->value) {
        /* This shouldn't happen. */
      }
    }

    node = avl_create_node();
    node->value = value;

    if (value < last->value)
      last->left = node;
    if (value > last->value)
      last->right = node;
  }

  avl_balance(tree);
}

avl_node_t *avl_find(avl_tree_t *tree, int value) {
  avl_node_t *current = tree->root;

  while (current && current->value != value) {
    if (value > current->value)
      current = current->right;
    else
      current = current->left;
  }

  return current;
}

int main(int argc, char **argv) {
  avl_tree_t tree;
  klee_make_symbolic(&tree, sizeof(tree), "tree");

  avl_insert(&tree, 2);
  avl_insert(&tree, 3);
  avl_insert(&tree, 1);
  if(avl_find(&tree, 2)) {
    return 1;
  } else {
    return 0;
  }
}

