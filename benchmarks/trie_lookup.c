/*

Copyright (c) 2005-2008, Simon Howard

Permission to use, copy, modify, and/or distribute this software
for any purpose with or without fee is hereby granted, provided
that the above copyright notice and this permission notice appear
in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

*/

#include <klee/klee.h>

#include <stdlib.h>
#include <string.h>

typedef struct _Trie Trie;
typedef struct _TrieNode TrieNode;

typedef void *TrieValue;

#define TRIE_NULL ((void *)0)

struct _TrieNode {
  TrieValue data;
  unsigned int use_count;
  TrieNode *next[256];
};

struct _Trie {
  TrieNode *root_node;
};


TrieValue trie_lookup_binary(Trie *trie, unsigned char *key, int key_length);

static TrieNode *trie_find_end(Trie *trie, char *key) {
  TrieNode *node;
  char *p;

  node = trie->root_node;

  for (p = key; *p != '\0'; ++p) {
    if (node == NULL) {
      return NULL;
    }
    node = node->next[(unsigned char)*p];
  }

  return node;
}

TrieValue trie_lookup(Trie *trie, char *key) {
  TrieNode *node;
  node = trie_find_end(trie, key);
  if (node != NULL) {
    return node->data;
  } else {
    return TRIE_NULL;
  }
}

int main() {
  Trie* trie;
  klee_make_symbolic(&trie, sizeof(trie), "trie");
  trie_lookup(trie, "slsl");
  return 0;
}
