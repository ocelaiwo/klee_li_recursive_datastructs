/*

Copyright (c) 2005-2008, Simon Howard

Permission to use, copy, modify, and/or distribute this software
for any purpose with or without fee is hereby granted, provided
that the above copyright notice and this permission notice appear
in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

*/

#include <klee/klee.h>

#include <stdlib.h>
#include <string.h>

typedef void *ArrayListValue;
typedef struct _ArrayList ArrayList;
struct _ArrayList {

	/** Entries in the array */
	ArrayListValue *data;

	/** Length of the array */
	unsigned int length;

	/** Private data and should not be accessed */
	unsigned int _alloced;
};


void arraylist_clear(ArrayList *arraylist)
{
	arraylist->length = 0;
}

int main() {
  ArrayList* arraylist;
  klee_make_symbolic(&arraylist, sizeof(arraylist), "arraylist");
  arraylist_clear(arraylist);
  if (arraylist->length == 0) {
    return 0;
  } else {
    return 1;
  }
}
