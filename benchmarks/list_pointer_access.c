#include "klee/klee.h"

struct List {
  struct List *next;
  int value;
};
typedef struct List List;

int main() {
  List *l;
  klee_make_symbolic(&l, sizeof(l), "l");

  if (l->next->next->value == 3) {
    return 1;
  } else {
    return 0;
  }
}
