#include "klee/klee.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum { RED, BLACK };

typedef int COLOR;
typedef int valueType;

typedef struct Node {
  valueType value;
  COLOR color;
  struct Node *right, *left, *parent;
} * node;

typedef node *tree;

node grandparent(node n) {
  if (n == NULL || n->parent == NULL)
    return NULL;
  return n->parent->parent;
}

int main(int argc, char *argv[]) {
  tree a;
  klee_make_symbolic(&a, sizeof(a), "a");
  grandparent(*a);
  return 0;
}
