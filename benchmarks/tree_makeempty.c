/* Binary Search Tree Implementation in C */
/* Harish R */

#include <klee/klee.h>

#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
  int data;
  struct TreeNode *left;
  struct TreeNode *right;
};

struct TreeNode *makeEmpty(struct TreeNode *root) {
  if (root != NULL) {
    makeEmpty(root->left);
    makeEmpty(root->right);
    free(root);
  }
  return NULL;
}

int main() {
  struct TreeNode *root;
  klee_make_symbolic(&root, sizeof(root), "root");
  makeEmpty(root);
}
