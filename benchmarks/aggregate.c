#include "klee/klee.h"

struct A {
  int a;
  int b;
  int c;
  int d;
};

typedef struct A A;

int sum(A a) {
  return a.a + a.b + a.c + a.d;
}

int main() {
  A a;
  klee_make_symbolic(&a, sizeof(a), "a");
  int b = sum(a);
  if (b > 30) {
    return 0;
  } else {
    return 1;
  }
}
