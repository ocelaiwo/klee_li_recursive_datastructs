#include "klee/klee.h"

int main() {
  int arr[10];
  klee_make_symbolic(arr, sizeof(arr), "arr");
  if (arr[3] == arr[5]) {
    return 0;
  } else {
    return 1;
  }
}
