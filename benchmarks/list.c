#include "klee/klee.h"

struct Node {
  struct Node *next;
  int value;
};
typedef struct Node Node;

int list_length(Node* a) {
  if(!a) {
    return 0;
  } else {
    return 1 + list_length(a->next);
  }
}

int main() {
  Node root;
  klee_make_symbolic(&root, sizeof(root), "root");
  if (list_length(&root) == 1 && root.value == 3) {
    return 1;
  } else {
    return 0;
  }
}
