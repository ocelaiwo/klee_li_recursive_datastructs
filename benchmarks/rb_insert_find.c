#include "klee/klee.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum { RED, BLACK };

typedef int COLOR;
typedef int valueType;

typedef struct Node {
  valueType value;
  COLOR color;
  struct Node *right, *left, *parent;
} * node;

typedef node *tree;

node initilize(node, valueType);
node grandparent(node);
node uncle(node);

void insertNode(tree, valueType);
void insertUtil(node);

void rotateRight(node);
void rotateLeft(node);

node findNode(tree, valueType);

node initilize(node p, valueType v) {
  node tree = (node)malloc(sizeof(struct Node));
  tree->left = tree->right = NULL;
  tree->parent = p;
  tree->value = v;
  tree->color = RED;
  return tree;
}

node grandparent(node n) {
  if (n == NULL || n->parent == NULL)
    return NULL;
  return n->parent->parent;
}

node uncle(node n) {
  node g = grandparent(n);
  if (n == NULL || g == NULL)
    return NULL;
  if (n->parent == g->left)
    return g->right;
  else
    return g->left;
}

void insertNode(tree t, valueType v) {
  int pl = 0;
  node ptr, btr = NULL, newNode;

  for (ptr = *t; ptr != NULL;
       btr = ptr, ptr = ((pl = (ptr->value > v)) ? ptr->left : ptr->right))
    ;

  newNode = initilize(btr, v);

  if (btr != NULL)
    (pl) ? (btr->left = newNode) : (btr->right = newNode);

  insertUtil(newNode);

  ptr = newNode;
  for (ptr = newNode; ptr != NULL; btr = ptr, ptr = ptr->parent)
    ;
  *t = btr;
}

void insertUtil(node n) {
  node u = uncle(n), g = grandparent(n), p = n->parent;
  if (p == NULL)
    n->color = BLACK;
  else if (p->color == BLACK)
    return;
  else if (u != NULL && u->color == RED) {
    p->color = BLACK;
    u->color = BLACK;
    g->color = RED;

    insertUtil(g);
  } else {
    if (n == p->right && p == g->left) {
      rotateLeft(p);
      n = n->left;
    } else if (n == p->left && p == g->right) {
      rotateRight(p);
      n = n->right;
    }

    g = grandparent(n);
    p = n->parent;

    p->color = BLACK;
    g->color = RED;

    if (n == p->left)
      rotateRight(g);
    else
      rotateLeft(g);
  }
}

void rotateRight(node tree) {
  node c = tree->left;
  node p = tree->parent;

  if (c->right != NULL)
    c->right->parent = tree;

  tree->left = c->right;
  tree->parent = c;
  c->right = tree;
  c->parent = p;

  if (p != NULL) {
    if (p->right == tree)
      p->right = c;
    else
      p->left = c;
  }
  printf("rotation %d, right\n", tree->value);
}
void rotateLeft(node tree) {
  node c = tree->right;
  node p = tree->parent;

  if (c->left != NULL)
    c->left->parent = tree;

  tree->right = c->left;
  tree->parent = c;
  c->left = tree;
  c->parent = p;

  if (p != NULL) {
    if (p->left == tree)
      p->left = c;
    else
      p->right = c;
  }
  printf("rotation %d, left\n", tree->value);
}

node findNode(tree t, valueType v) {
  node p;
  for (p = *t; p != NULL && p->value != v;
       p = (p->value > v ? p->left : p->right))
    ;
  return p;
}

int main(int argc, char *argv[]) {
  tree a;
  klee_make_symbolic(&a, sizeof(a), "a");
  insertNode(a, 3);
  if (findNode(a, 3)) {
    return 0;
  } else {
    return 1;
  }
}
