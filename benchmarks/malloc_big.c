#include "klee/klee.h"
#include <stdlib.h>

struct A {
  int a;
  int b;
};
typedef struct A A;

int main() {
  A* a = malloc(sizeof(A));
  klee_make_symbolic(a, sizeof(A), "*a");
  if (a->a == a->b) {
    return 0;
  } else {
    return 1;
  }
}
