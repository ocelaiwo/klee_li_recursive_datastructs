#include "klee/klee.h"

int main() {
  int a;
  int b;
  int c;
  klee_make_symbolic(&a, sizeof(a), "a");
  klee_make_symbolic(&b, sizeof(b), "b");
  klee_make_symbolic(&c, sizeof(c), "c");
  if (a > 10) {
    if (b < 10) {
      if (a + b > 100) {
        if (c == 3) {
          return 0;
        }
        return 1;
      }
      return 2;
    }
    return 3;
  }
  return 4;
}
