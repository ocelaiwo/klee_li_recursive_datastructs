#include "klee/klee.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum { RED, BLACK };

typedef int COLOR;
typedef int valueType;

typedef struct Node {
  valueType value;
  COLOR color;
  struct Node *right, *left, *parent;
} * node;

typedef node *tree;

void destroy(node tree) {
  if (tree == NULL)
    return;
  destroy(tree->left);
  destroy(tree->right);
  free(tree);
}

int main(int argc, char *argv[]) {
  tree a;
  klee_make_symbolic(&a, sizeof(a), "a");
  destroy((*a)->left);
  return 0;
}
