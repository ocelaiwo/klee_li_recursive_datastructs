#include "klee/klee.h"

#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct avl_node_s {
  struct avl_node_s *left;
  struct avl_node_s *right;
  int value;
};

typedef struct avl_node_s avl_node_t;

struct avl_tree_s {
  struct avl_node_s *root;
};

typedef struct avl_tree_s avl_tree_t;

avl_node_t *avl_find(avl_tree_t *tree, int value) {
  avl_node_t *current = tree->root;

  while (current && current->value != value) {
    if (value > current->value)
      current = current->right;
    else
      current = current->left;
  }

  return current;
}

int main(int argc, char **argv) {
  avl_tree_t tree;
  klee_make_symbolic(&tree, sizeof(tree), "tree");

  if(avl_find(&tree, 2)) {
    return 1;
  } else {
    return 0;
  }
}
