#include "klee/klee.h"

struct List {
  struct List *next;
  int value;
};
typedef struct List List;

int length(List *l) {
  if (!l) {
    return 0;
  }
  return 1 + length(l->next);
}

int main() {
  List *l;
  klee_make_symbolic(&l, sizeof(l), "l");
  if (length(l) == 3) {
    return 0;
  } else {
    return 1;
  }
}
