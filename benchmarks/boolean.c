#include <klee/klee.h>

int main() {
  int a, b, c, d;
  klee_make_symbolic(&a, sizeof(a), "a");
  klee_make_symbolic(&b, sizeof(b), "b");
  klee_make_symbolic(&c, sizeof(c), "c");
  klee_make_symbolic(&d, sizeof(d), "d");
  if(a && b && !c && d) {
    return 1;
  } else {
    return 0;
  }
}
