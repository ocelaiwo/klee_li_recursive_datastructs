/* Binary Search Tree Implementation in C */
/* Harish R */

#include <klee/klee.h>

#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
  int data;
  struct TreeNode *left;
  struct TreeNode *right;
};

int inorder(struct TreeNode *root) {
  if (root == NULL)
    return 0;
  return inorder(root->left) + inorder(root->right) + 1;
}

int main() {
  struct TreeNode *root;
  klee_make_symbolic(&root, sizeof(root), "root");
  int count = inorder(root);
  if (count == 2) {
    return 1;
  } else {
    return 0;
  }
}
