#include "klee/klee.h"

struct A {
  int a;
  int b;
};
typedef struct A A;

struct B {
  A a;
  int b;
  A c;
};
typedef struct B B;

int main() {
  A a;
  B b;
  klee_make_symbolic(&a, sizeof(a), "a");
  klee_make_symbolic(&b, sizeof(b), "b");
  b.a = a;
  a.a = 3;
  a.b = b.b;
  if (a.a + a.b + b.b + b.c.a + b.c.b == 40) {
    return 0;
  } else {
    return 1;
  }
}
