/*

Copyright (c) 2005-2008, Simon Howard

Permission to use, copy, modify, and/or distribute this software
for any purpose with or without fee is hereby granted, provided
that the above copyright notice and this permission notice appear
in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

 */

#include <klee/klee.h>

#include <stdlib.h>

typedef struct _Queue Queue;
typedef struct _QueueEntry QueueEntry;

typedef void *QueueValue;

#define QUEUE_NULL ((void *)0)

struct _QueueEntry {
  QueueValue data;
  QueueEntry *prev;
  QueueEntry *next;
};

struct _Queue {
  QueueEntry *head;
  QueueEntry *tail;
};

int queue_push_head(Queue *queue, QueueValue data);
QueueValue queue_pop_head(Queue *queue);
int queue_is_empty(Queue *queue);

int queue_push_head(Queue *queue, QueueValue data) {
  QueueEntry *new_entry;

  new_entry = malloc(sizeof(QueueEntry));

  if (new_entry == NULL) {
    return 0;
  }

  new_entry->data = data;
  new_entry->prev = NULL;
  new_entry->next = queue->head;

  if (queue->head == NULL) {
    queue->head = new_entry;
    queue->tail = new_entry;
  } else {
    queue->head->prev = new_entry;
    queue->head = new_entry;
  }

  return 1;
}

QueueValue queue_pop_head(Queue *queue) {
  QueueEntry *entry;
  QueueValue result;

  if (queue_is_empty(queue)) {
    return QUEUE_NULL;
  }

  entry = queue->head;
  queue->head = entry->next;
  result = entry->data;

  if (queue->head == NULL) {
    queue->tail = NULL;
  } else {
    queue->head->prev = NULL;
  }

  free(entry);

  return result;
}

int queue_is_empty(Queue *queue) { return queue->head == NULL; }

int main() {
  Queue* q;
  klee_make_symbolic(&q, sizeof(q), "q");
  int a = 3;
  queue_push_head(q, &a);
  queue_pop_head(q);
  if(queue_is_empty(q)) {
    return 1;
  } else {
    return 0;
  }
}
