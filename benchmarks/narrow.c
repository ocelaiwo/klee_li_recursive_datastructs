#include "klee/klee.h"

int main() {
  int a, b;
  klee_make_symbolic(&a, sizeof(a), "a");
  klee_make_symbolic(&b, sizeof(b), "b");
  if (a > 10 && a < 20 && b > 10 && b < 10) {
    if (a + b < 20 || a + b > 40) {
      return 1;
    } else {
      return 0;
    }
  }
}
