/*

Copyright (c) 2005-2008, Simon Howard

Permission to use, copy, modify, and/or distribute this software
for any purpose with or without fee is hereby granted, provided
that the above copyright notice and this permission notice appear
in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

*/

#include <klee/klee.h>

#include <stdlib.h>
#include <string.h>

typedef struct _Trie Trie;
typedef struct _TrieNode TrieNode;

typedef void *TrieValue;

#define TRIE_NULL ((void *)0)

struct _TrieNode {
  TrieValue data;
  unsigned int use_count;
  TrieNode *next[256];
};

struct _Trie {
  TrieNode *root_node;
};


int trie_insert(Trie *trie, char *key, TrieValue value);
int trie_remove(Trie *trie, char *key);

static TrieNode *trie_find_end(Trie *trie, char *key) {
  TrieNode *node;
  char *p;

  node = trie->root_node;

  for (p = key; *p != '\0'; ++p) {
    if (node == NULL) {
      return NULL;
    }
    node = node->next[(unsigned char)*p];
  }

  return node;
}

static void trie_insert_rollback(Trie *trie, unsigned char *key) {
  TrieNode *node;
  TrieNode **prev_ptr;
  TrieNode *next_node;
  TrieNode **next_prev_ptr;
  unsigned char *p;

  node = trie->root_node;
  prev_ptr = &trie->root_node;
  p = key;

  while (node != NULL) {
    next_prev_ptr = &node->next[(unsigned char)*p];
    next_node = *next_prev_ptr;
    ++p;

    --node->use_count;

    if (node->use_count == 0) {
      free(node);

      if (prev_ptr != NULL) {
        *prev_ptr = NULL;
      }

      next_prev_ptr = NULL;
    }

    node = next_node;
    prev_ptr = next_prev_ptr;
  }
}

int trie_insert(Trie *trie, char *key, TrieValue value) {
  TrieNode **rover;
  TrieNode *node;
  char *p;
  int c;

  if (value == TRIE_NULL) {
    return 0;
  }

  node = trie_find_end(trie, key);

  if (node != NULL && node->data != TRIE_NULL) {
    node->data = value;
    return 1;
  }

  rover = &trie->root_node;
  p = key;

  for (;;) {
    node = *rover;
    if (node == NULL) {
      node = (TrieNode *)calloc(1, sizeof(TrieNode));
      if (node == NULL) {
        trie_insert_rollback(trie, (unsigned char *)key);
        return 0;
      }
      node->data = TRIE_NULL;
      *rover = node;
    }

    ++node->use_count;
    c = (unsigned char)*p;

    if (c == '\0') {
      node->data = value;
      break;
    }
    rover = &node->next[c];
    ++p;
  }
  return 1;
}

int trie_remove(Trie *trie, char *key) {
  TrieNode *node;
  TrieNode *next;
  TrieNode **last_next_ptr;
  char *p;
  int c;

  node = trie_find_end(trie, key);

  if (node != NULL && node->data != TRIE_NULL) {
    node->data = TRIE_NULL;
  } else {
    return 0;
  }

  node = trie->root_node;
  last_next_ptr = &trie->root_node;
  p = key;

  for (;;) {
    c = (unsigned char)*p;
    next = node->next[c];
    --node->use_count;
    if (node->use_count <= 0) {
      free(node);
      if (last_next_ptr != NULL) {
        *last_next_ptr = NULL;
        last_next_ptr = NULL;
      }
    }

    if (c == '\0') {
      break;
    } else {
      ++p;
    }

    if (last_next_ptr != NULL) {
      last_next_ptr = &node->next[c];
    }

    node = next;
  }

  return 1;
}

int main() {
  Trie *trie;
  klee_make_symbolic(&trie, sizeof(trie), "trie");
  trie_insert(trie, "slsl", 0);
  trie_remove(trie, "slsl");
  return 0;
}
