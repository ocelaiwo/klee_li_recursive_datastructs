/* Binary Search Tree Implementation in C */
/* Harish R */

#include <klee/klee.h>

#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
  int data;
  struct TreeNode *left;
  struct TreeNode *right;
};

struct TreeNode *findMin(struct TreeNode *root) {
  if (root == NULL)
    return NULL;
  else if (root->left == NULL)
    return root;
  else
    return findMin(root->left);
}

struct TreeNode *find(struct TreeNode *root, int x) {
  if (root == NULL)
    return NULL;
  else if (x < root->data)
    return find(root->left, x);
  else if (x > root->data)
    return find(root->right, x);
  else
    return root;
}

struct TreeNode *delete(struct TreeNode *root, int x) {
  struct TreeNode *temp;
  if (root == NULL)
    return NULL;
  else if (x < root->data)
    root->left = delete(root->left, x);
  else if (x > root->data)
    root->right = delete(root->right, x);
  else if (root->left && root->right) {
    temp = findMin(root->right);
    root->data = temp->data;
    root->right = delete(root->right, root->data);
  } else {
    temp = root;
    if (root->left == NULL)
      root = root->right;
    else if (root->right == NULL)
      root = root->left;
    free(temp);
  }
  return root;
}

int main() {
  struct TreeNode *root;
  klee_make_symbolic(&root, sizeof(root), "root");
  delete(root, 3);
  if (find(root, 3)) {
    return 0;
  } else {
    return 1;
  }
}
