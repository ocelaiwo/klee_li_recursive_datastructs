#include "klee/klee.h"

int main() {
  int *a;
  int b;
  klee_make_symbolic(&a, sizeof(a), "a");
  klee_make_symbolic(&b, sizeof(b), "b");

  if (b > 0) {
    *a = 5;
  } else {
    *a = 3;
  }

  if (*a == 5 && b > 0 && b != 5) {
    return 0;
  } else {
    return 1;
  }
}
