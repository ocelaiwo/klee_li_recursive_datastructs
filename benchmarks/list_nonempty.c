#include "klee/klee.h"
#include <stdlib.h>

struct List {
  struct List *next;
  int value;
};
typedef struct List List;

int main() {
  List *l;
  klee_make_symbolic(&l, sizeof(l), "l");

  List *cur = l;
  while (cur->next != NULL) {
    cur = cur->next;
  }
  cur->next = malloc(sizeof(List));
  cur->next->next = NULL;
  cur = l;
  int count = 0;
  while (cur->next != NULL) {
    count++;
    cur = cur->next;
  }
  if (count == 3) {
    return 0;
  } else {
    return 1;
  }
}
