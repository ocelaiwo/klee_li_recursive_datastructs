#include "klee/klee.h"
#include <stdlib.h>

int main() {
  int *a = malloc(sizeof(int));
  klee_make_symbolic(a, sizeof(int), "*a");
  if (*a == 10) {
    return 0;
  } else {
    return 1;
  }
}
