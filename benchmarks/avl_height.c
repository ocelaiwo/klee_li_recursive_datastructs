#include "klee/klee.h"

#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct avl_node_s {
  struct avl_node_s *left;
  struct avl_node_s *right;
  int value;
};

typedef struct avl_node_s avl_node_t;

struct avl_tree_s {
  struct avl_node_s *root;
};

typedef struct avl_tree_s avl_tree_t;

int avl_node_height(avl_node_t *node) {
  int height_left = 0;
  int height_right = 0;

  if (node->left)
    height_left = avl_node_height(node->left);
  if (node->right)
    height_right = avl_node_height(node->right);

  return height_right > height_left ? ++height_right : ++height_left;
}

int main(int argc, char **argv) {
  avl_tree_t tree;
  klee_make_symbolic(&tree, sizeof(tree), "tree");

  if(avl_node_height(tree.root) == 3) {
    return 0;
  } else {
    return 1;
  }
}
