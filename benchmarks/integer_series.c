#include "klee/klee.h"

#include <assert.h>

struct Node {
  int value;
  struct Node *next;
};
typedef struct Node Node;

int main() {
  Node *root;
  klee_make_symbolic(&root, sizeof(root), "root");
  Node *node = root;
  for (int i = 0; i < 5; i++) {
    node->value = i;
    node = node->next;
  }
  int sum = 0;
  node = root;
  for (int i = 0; i < 3; i++) {
    sum += node->value;
    node = node->next;
  }
  assert(sum == 0 + 1 + 2);
  return 0;
}
