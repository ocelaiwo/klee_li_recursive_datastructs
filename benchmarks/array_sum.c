#include "klee/klee.h"

int main() {
  int arr[10];
  klee_make_symbolic(arr, sizeof(arr), "arr");
  int sum = 0;
  for (int i = 0; i < 10; i++) {
    sum += arr[i];
  }
  if (sum > 25) {
    return 0;
  } else {
    return 1;
  }
}
