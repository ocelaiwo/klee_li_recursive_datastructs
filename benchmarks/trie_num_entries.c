/*

Copyright (c) 2005-2008, Simon Howard

Permission to use, copy, modify, and/or distribute this software
for any purpose with or without fee is hereby granted, provided
that the above copyright notice and this permission notice appear
in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

*/

#include <klee/klee.h>

#include <stdlib.h>
#include <string.h>

typedef struct _Trie Trie;
typedef struct _TrieNode TrieNode;

typedef void *TrieValue;

#define TRIE_NULL ((void *)0)

struct _TrieNode {
  TrieValue data;
  unsigned int use_count;
  TrieNode *next[256];
};

struct _Trie {
  TrieNode *root_node;
};

unsigned int trie_num_entries(Trie *trie) {
  if (trie->root_node == NULL) {
    return 0;
  } else {
    return trie->root_node->use_count;
  }
}

int main() {
  Trie* trie;
  klee_make_symbolic(&trie, sizeof(trie), "trie");
  if (trie_num_entries(trie) == 3) {
    return 1;
  } else {
    return 0;
  }
}
