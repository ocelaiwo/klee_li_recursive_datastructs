#include "klee/klee.h"

int f(int p) {
  if (p > 0) {
    if (p == 13) {
      return 1;
    } else {
      return f(p - 1);
    }
  } else if (p == 0) {
    return 0;
  } else {
    return -1;
  }
}

int main() {
  int x;
  klee_make_symbolic(&x, sizeof(x), "x");
  int ret = f(x);
  return ret;
}
