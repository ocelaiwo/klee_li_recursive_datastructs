/*

Copyright (c) 2005-2008, Simon Howard

Permission to use, copy, modify, and/or distribute this software
for any purpose with or without fee is hereby granted, provided
that the above copyright notice and this permission notice appear
in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

 */

#include <klee/klee.h>

#include <stdlib.h>

typedef struct _Queue Queue;
typedef struct _QueueEntry QueueEntry;

typedef void *QueueValue;

#define QUEUE_NULL ((void *)0)

struct _QueueEntry {
  QueueValue data;
  QueueEntry *prev;
  QueueEntry *next;
};

struct _Queue {
  QueueEntry *head;
  QueueEntry *tail;
};

QueueValue queue_peek_head(Queue *queue);
QueueValue queue_peek_tail(Queue *queue);
int queue_is_empty(Queue *queue);


QueueValue queue_peek_head(Queue *queue) {
  if (queue_is_empty(queue)) {
    return QUEUE_NULL;
  } else {
    return queue->head->data;
  }
}

QueueValue queue_peek_tail(Queue *queue) {
  if (queue_is_empty(queue)) {
    return QUEUE_NULL;
  } else {
    return queue->tail->data;
  }
}

int queue_is_empty(Queue *queue) { return queue->head == NULL; }

int main() {
  Queue* q;
  klee_make_symbolic(&q, sizeof(q), "q");
  if(queue_peek_head(q) == queue_peek_tail(q)) {
    return 1;
  } else {
    return 0;
  }
}
