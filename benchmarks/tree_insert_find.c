/* Binary Search Tree Implementation in C */
/* Harish R */

#include <klee/klee.h>

#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
  int data;
  struct TreeNode *left;
  struct TreeNode *right;
};

struct TreeNode *insert(struct TreeNode *root, int x) {
  if (root == NULL) {
    root = malloc(sizeof(struct TreeNode));
    root->data = x;
    root->left = root->right = NULL;
  } else if (x < root->data)
    root->left = insert(root->left, x);
  else if (x > root->data)
    root->right = insert(root->right, x);
  return root;
}

struct TreeNode *find(struct TreeNode *root, int x) {
  if (root == NULL)
    return NULL;
  else if (x < root->data)
    return find(root->left, x);
  else if (x > root->data)
    return find(root->right, x);
  else
    return root;
}

int main() {
  struct TreeNode *root;
  klee_make_symbolic(&root, sizeof(root), "root");
  insert(root, 3);
  insert(root, 5);
  if (find(root, 5)) {
    return 1;
  } else {
    return 0;
  }
}
