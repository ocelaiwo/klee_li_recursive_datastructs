#include <klee/klee.h>

int abs(int x) {
  if (x >= 0) {
    return x;
  } else {
    return -x;
  }
}

int main() {
  int x;
  klee_make_symbolic(&x, sizeof(x), "x");
  if (abs(x) < 0) {
    return 0;
  } else {
    return 1;
  }
}
