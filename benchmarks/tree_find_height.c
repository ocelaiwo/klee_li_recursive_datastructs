/* Binary Search Tree Implementation in C */
/* Harish R */

#include <klee/klee.h>

#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
  int data;
  struct TreeNode *left;
  struct TreeNode *right;
};

int findHeight(struct TreeNode *root) {
  int lefth, righth;
  if (root == NULL)
    return -1;
  lefth = findHeight(root->left);
  righth = findHeight(root->right);
  return (lefth > righth ? lefth : righth) + 1;
}

int main() {
  struct TreeNode *root;
  klee_make_symbolic(&root, sizeof(root), "root");
  if (findHeight(root) == 3) {
    return 1;
  } else {
    return 0;
  }
}
